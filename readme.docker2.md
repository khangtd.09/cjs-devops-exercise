# Docker (Part 2)

We love docker. Docker is lovely.

You can take a quick look at here http://www.zdnet.com/article/what-is-docker-and-why-is-it-so-darn-popular/

# General steps

1. OS requirement: Linux, for example: https://elementary.io/
2. Make sure you have installed docker and docker-compose https://docs.docker.com/compose/install/

# Exercises

##### 4. You already had your own repo with branch `dev-docker-1`

##### 5. Get next step into the production, we need a bit more flexible docker system. Checkout to new branch `dev-docker-2` based on the `dev-docker-1`. 

We already have a repo that can start Django in production mode easily. 
But the issues here were everytime we need some updated versions of the app, we have to rebuild the image again, and deliver them to customers' servers, which was very time-consuming.
The media files, database of PostgreSQL and database of Redis are the most important things we need to backup.
So what we need is a repo that contains:

- Modified Dockerfile that allow us to store django app source code, media files, database PostgreSQL and database Redis outside of the app container. Make them independently and easy to maintain. (docker-compose is suggested in this case, use it wisely)

- A bash script that we can backup/restore database of PostgreSQL.

- A script bash that we can start/stop/restart whole system using docker-compose.

##### 6. Commit your repo and push to branch `dev-docker-2`, we think that you can re-structure your source code a little bit, to make it more clear and easy to look at.


[<< To Previous](./readme.docker.md)  |  [To Next >>](./readme.iis.md)
