# Docker (Part 1)

We love docker. Docker is lovely.

You can take a quick look at here http://www.zdnet.com/article/what-is-docker-and-why-is-it-so-darn-popular/

# General steps

1. OS requirement: Linux, for example: https://elementary.io/
2. Make sure you have installed docker and docker-compose https://docs.docker.com/compose/install/

# Exercises

##### 1. Please reference the project here https://github.com/ploggingdev/djangochat, also you may need this https://github.com/dockerfiles/django-uwsgi-nginx.

##### 2. After you have learned about the projects, we need a docker git repo that contains:


- Dockerfile that successfully build the Django Chat app in the production mode (Redis, PostgreSQL - any dependencies you need, you can bundle into Dockerfile)


- A bash script that we can access into the bash of container.


##### 3. Commit your repo and push to branch `dev-docker-1`




[<< To Previous](./README.md)  |  [To Next >>](./readme.docker2.md)
