# Cloudjet Solutions - DevOps Excerises


Congratulations for having done a great job.

We are almost close to you. One more step and then you can be a part of us, one the most enthusiastic challengers of the world.

<div style="display: block; margin-bottom: 30px;">
	
<img style="float: right; display: block; margin-right: 30px;" src="http://88.cjs.vn/imgs/___Organization___Cloudjet%20Corp.___2746f581-d03a-4327-92cc-1d0dc4c5c94e.png" data-canonical-src="http://88.cjs.vn/imgs/___Organization___Cloudjet%20Corp.___2746f581-d03a-4327-92cc-1d0dc4c5c94e.png" width="300" />

</div>



<div style="display:block; margin-bottom: 80px; height: 10px">

</div>


<div style="display:block; margin-bottom: 80px; height: 10px">

</div>

# One more step you need to do


Submit your application form and notify your guide that you have finished the exercises.


Ps: You can click [Start](./readme.docker.md) to start over the exercises.





<div style="display:block; margin-bottom: 80px; height: 10px">

</div>


# About us

##### We encourage our people when feel lost motivations

1. Think back why you were here, what you have achieved
2. Think about changing into new positions
3. Talk with your managers, feel free addressing what you think. No fear. 



##### Some hints for you to know about us

1. Website: http://www.cloudjetsolutions.com/ | https://cjs.vn
2. FB: https://www.facebook.com/cloudjetsolutions/
3. Google search: [Click here](https://www.google.com.vn/search?client=ubuntu&hs=Bqf&ei=p4HEWtXbMoK-0gTl6JmgBQ&q=Cloudjet+Solutions&oq=Cloudjet+Solutions&gs_l=psy-ab.3..0j0i203k1j0i22i30k1j0i203k1j38.769.5217.0.5342.32.19.12.0.0.0.184.2080.3j15.18.0....0...1.1.64.psy-ab..2.30.2159...46j35i39k1j0i131k1j0i67k1j0i46i67k1j46i67k1j0i46k1j0i3k1j0i10k1j0i30k1j0i22i10i30k1.0.LcKemM37Ha0)

##### We apply these main dependencies in the systems

1. Django
2. Postgres
3. Docker
4. Heroku
5. IIS

and many more things we have applied. Nothing was impossible, even modifiying library source code. 