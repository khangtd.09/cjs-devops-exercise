# Windows IIS - Webserver

Yes, we are talking about deploying a Django app on multiple platforms, to serve multiple demands of customers. There are some customers will demand us like "Hey, could you please deploy an app into our licensed registered Windows Server?"
We are always ready to say "Yes", of course.

# General Steps

1. We reccomend you could have a look at this blog http://blog.mattwoodward.com/2016/07/running-django-application-on-windows.html
2. We also need a SQL Server database, so you could need this, too. https://github.com/michiya/django-pyodbc-azure
3. So you can create a VM with Windows Server 2012 R2, also need to install SQL Server 2012.
4. Get the repo of Django Chat app, https://github.com/ploggingdev/djangochat
5. When you done preparing, we are about to start the exercises.

# Exercises

##### 1. Checkout from your master branch, into new branch `dev-iis`

##### 2. Commit your repo which successfully run Django Chat in combo Windows Server 2012 R2 + SQL Server 2012
If you have passed the Docker exercises, there should be no more guide for this exercises. One thing we require that you should use python-dotenv to store the environment variables.

##### 3. An installation document (tell us how you do to start the system successfully) should be included with your repo.

##### 4. Push your code with installation document into `dev-iis`.



[<< To Previous](./readme.electron.md)  |  [To Next >>](./submit.md)
